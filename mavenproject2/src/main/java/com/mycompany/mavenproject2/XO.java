/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.mavenproject2;

import java.util.Scanner;

/**
 *
 * @author zacop
 */
public class XO {

    static char player = 'X';
    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static int row, col;
    static int x = 0;

    public static void printXOGame(){
        System.out.println("OX Game");
    }
    
    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input Row and Col: ");
        row = sc.nextInt();
        col = sc.nextInt();
    }

    public static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    public static void pressPlayer() {
        if (table[row][col] != 'X' && table[row][col] != 'O') {
            table[row][col] = player;
        } else {
            System.out.println("ERORR: Please select a space");
            inputRowCol();
            pressPlayer();
        }
    }

    public static boolean checkX1() {
        if (table[1][1] != '-') {
            if (table[0][0] == table[1][1] && table[1][1] == table[2][2]) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkX2() {
        if (table[1][1] != '-') {
            if (table[0][2] == table[1][1] && table[1][1] == table[2][0]) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkAllX() {
        //checkX1();
        //checkX2();
        if (checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    public static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == player && table[i][1] == player && table[i][2] == player) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == player && table[1][i] == player && table[2][i] == player) {
                return true;
            }
        }
        return false;
    }

    public static void checkWin() {
        if (checkAllX() || checkRow() || checkCol() == true) {
            showTable();
            System.out.println("Player " + player + " Win!!");
            x = 1;
        }
    }
    
    public static void printTurn(){
        System.out.println(player+" Turn");
    }
    
    public static void resetTable(){
        table = new char[][] { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
    }
    
    public static String playAgain(){
        Scanner sc = new Scanner (System.in);
        String c = sc.next();   
        return c;
    }
    
    public static void printContinue(){
        System.out.println("continue (y/n): ");
    }

    public static void main(String[] args) {
        printXOGame();
        for (int i = 0; i < 9; i++) {
            showTable();
            printTurn();
            inputRowCol();
            pressPlayer();
            checkWin();
            if (x == 1) {
                i = 9;
                printContinue();
                String conti = playAgain();
                if(conti.equals("y")){
                    x = 0;
                    i = -1;
                    resetTable();
                    player = 'O';
                }else{
                    break;
                }
            }
            switchPlayer();
            if (i == 8) {
                showTable();
                System.out.println("Draw!!");
                printContinue();
                String conti = playAgain();
                if(conti.equals("y")){
                    x = 0;
                    i = -1;
                    resetTable();
                    player = 'X';
                }else{
                    break;
                }
            }
        }
    }
}
